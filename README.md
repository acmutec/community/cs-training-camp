# Mathematics for Computer Science: Study Group

This is the git repository for the material that will be used and produced throughout the study sessions. 

+ The check the syllabus [click here](./syllabus.pdf).
+ The books will lay in `resources/books/`. They are encrypted, but we'll provide the passwords on the first day of this course.

Have fun and read the syllabus, it's important to have a get an idea of which topics will be covered. This will be tough, but don't give up! :^)

## ChangeLog

+ `[19/07/2019, 13:12]` Major changes in Methodology and the syllabus structure (included new feedback of Prof. José Miguel Renom). Induction now comes in the first section and more emphasis is put on the first part of the course. Estimated time per unit has changed. Added [GAL11] reference.
